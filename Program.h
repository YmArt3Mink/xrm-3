#pragma once

#include "MailMessageBuilder.h"
#include "AwkwardMailMessageBuilder.h"
#include "MainMessageDirector.h"


class Program
{
private:
	static Program* _pInstance;

	MailMessageDirector* _pDirector;

private:
	Program() {}

public:
	~Program() {}

	static Program* 
	GetInstance()
	{
		if (_pInstance == nullptr)
		{
			_pInstance = new Program();
		}

		return _pInstance;
	}

	void
	BuildWith(AbstractMailMessageBuilder* pBuilder)
	{
		std::vector<std::string> vRecipients;
		std::string				 sBody;
		std::vector<std::string> vRecipientsCopy;
		std::string				 sSubject;

		vRecipients.push_back("a@b.ru");
		vRecipients.push_back("b@c.ru");

		vRecipientsCopy.push_back("c@d.ru");
		vRecipientsCopy.push_back("d@e.ru");

		sBody = "body";
		sSubject = "subject";

		_pDirector = new MailMessageDirector(pBuilder);
		_pDirector->Build(
			vRecipients,
			sBody,
			vRecipientsCopy,
			sSubject
			);

		auto pMessage = _pDirector->GetResult();
		pMessage->Show();
	}

	void
	Run()
	{
		BuildWith(new MailMessageBuilder());
		BuildWith(new AwkwardMailMessageBuilder());
	}
};

