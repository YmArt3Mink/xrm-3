#pragma once

#include "AbstractMailMessageBuilder.h"


class MailMessageBuilder : public AbstractMailMessageBuilder
{
private:
	std::vector<std::string> _vRecipients;
	std::string				 _sBody;
	std::vector<std::string> _vRecipientsCopy;
	std::string				 _sSubject;

public:
	MailMessageBuilder() {}
	virtual ~MailMessageBuilder() {}

public:
	virtual AbstractMailMessageBuilder*
	AddRecipient(std::string sRecipient)
	{
		_vRecipients.push_back(sRecipient);
		return this;
	}

	virtual AbstractMailMessageBuilder*
	ClearRecipients()
	{
		_vRecipients.clear();
		return this;
	}

	virtual AbstractMailMessageBuilder*
	AddRecipientCopy(std::string sRecipient)
	{
		_vRecipientsCopy.push_back(sRecipient);
		return this;
	}

	virtual AbstractMailMessageBuilder*
	ClearRecipientsCopy()
	{
		_vRecipientsCopy.clear();
		return this;
	}

	virtual AbstractMailMessageBuilder*
	SetBody(std::string sBody)
	{
		_sBody = sBody;
		return this;
	}

	virtual AbstractMailMessageBuilder*
	SetSubject(std::string sSubject)
	{
		_sSubject = sSubject;
		return this;
	}

	virtual MailMessage*
	GetResult()
	{
		auto pMessage =
			MailMessage::CreateMessage(
				_vRecipients,
				_sBody,
				_vRecipientsCopy,
				_sSubject
				);

		return pMessage;
	}

};