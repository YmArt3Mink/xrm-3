#pragma once

#include <string>

#include "AbstractMailMessageBuilder.h"
#include "StringUtils.h"


class AwkwardMailMessageBuilder : public AbstractMailMessageBuilder
{
private:
	std::string _sRecipients;
	std::string	_sBody;
	std::string _sRecipientsCopy;
	std::string	_sSubject;

public:
	AwkwardMailMessageBuilder() {}
	virtual ~AwkwardMailMessageBuilder() {}

public:
	virtual AbstractMailMessageBuilder*
	AddRecipient(std::string sRecipient)
	{
		_sRecipients += sRecipient + ';';
		return this;
	}

	virtual AbstractMailMessageBuilder*
	ClearRecipients()
	{
		_sRecipients.clear();
		return this;
	}

	virtual AbstractMailMessageBuilder*
	AddRecipientCopy(std::string sRecipient)
	{
		_sRecipientsCopy += sRecipient + ';';
		return this;
	}

	virtual AbstractMailMessageBuilder*
	ClearRecipientsCopy()
	{
		_sRecipientsCopy.clear();
		return this;
	}

	virtual AbstractMailMessageBuilder*
	SetBody(std::string sBody)
	{
		_sBody = sBody;
		return this;
	}

	virtual AbstractMailMessageBuilder*
	SetSubject(std::string sSubject)
	{
		_sSubject = sSubject;
		return this;
	}

	virtual MailMessage*
	GetResult()
	{
		auto vRecipients = StringUtils::Split(_sRecipients, ';');
		auto vRecipientsCopy = StringUtils::Split(_sRecipientsCopy, ';');
		auto pMessage =
			MailMessage::CreateMessage(
				vRecipients,
				_sBody,
				vRecipientsCopy,
				_sSubject
				);

		return pMessage;
	}

};