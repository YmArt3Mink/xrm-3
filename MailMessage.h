#pragma once

#include <iostream>
#include <algorithm>
#include <string>
#include <vector>


class MailMessage
{
private:
	std::vector<std::string> _vRecipients;
	std::string				 _sBody;
	std::vector<std::string> _vRecipientsCopy;
	std::string				 _sSubject;

private:
	MailMessage() {}

	MailMessage(const MailMessage& message) 
	{
		_vRecipients	 = message._vRecipients;
		_sBody			 = message._sBody;
		_vRecipientsCopy = message._vRecipientsCopy;
		_sSubject		 = message._sSubject;
	}

	MailMessage(MailMessage&& message) 
	{
		_vRecipients	 = std::move(message._vRecipients);
		_sBody			 = std::move(message._sBody);
		_vRecipientsCopy = std::move(message._vRecipientsCopy);
		_sSubject		 = std::move(message._sSubject);
	}

	~MailMessage() {}

	friend class MailMessageBuilder;
	friend class AwkwardMailMessageBuilder;

private:
	static MailMessage*
	CreateMessage(
		std::vector<std::string> vRecipients,
		std::string				 sBody,
		std::vector<std::string> vRecipientsCopy,
		std::string				 sSubject
		)
	{
		auto pMessage = new MailMessage();
		pMessage->_vRecipients	   = vRecipients;
		pMessage->_sBody		   = sBody;
		pMessage->_vRecipientsCopy = vRecipientsCopy;
		pMessage->_sSubject		   = sSubject;

		return pMessage;
	}

	static MailMessage*
	CreateMessage(MailMessage* pMessage)
	{
		return new MailMessage(*pMessage);
	}

public:
	void
	Show()
	{
		std::cout << "\nSubject: " << _sSubject;
		std::cout << "\nBody: "	   <<    _sBody;

		std::cout << "\nRecipients: ";
		std::for_each(_vRecipients.begin(), _vRecipients.end(),
			[this] (std::string sRecipient)
		{
			std::cout << sRecipient << ";";
		});

		std::cout << "\nRecipientsCopy: ";
		std::for_each(_vRecipientsCopy.begin(), _vRecipientsCopy.end(),
			[this] (std::string sRecipient)
		{
			std::cout << sRecipient << ";";
		});

		std::cout << std::endl;
	}
};