#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <sstream>


class StringUtils
{
public:
	static std::vector<std::string> 
	Split(std::string sStr, char delim)
	{
		std::vector<std::string> vTemp;
		std::stringstream ssTemp(sStr);
		std::string token;

		while (std::getline(ssTemp, token, delim))
		{
			vTemp.push_back(token);
		}

		return vTemp;
	}

};