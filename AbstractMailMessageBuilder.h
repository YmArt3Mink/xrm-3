#pragma once

#include <string>

#include "MailMessage.h"


class AbstractMailMessageBuilder
{
public:
	virtual ~AbstractMailMessageBuilder() {}
	
public:
	virtual AbstractMailMessageBuilder*
	AddRecipient(std::string sRecipient) = 0;

	virtual AbstractMailMessageBuilder*
	ClearRecipients() = 0;

	virtual AbstractMailMessageBuilder*
	AddRecipientCopy(std::string sRecipient) = 0;

	virtual AbstractMailMessageBuilder*
	ClearRecipientsCopy() = 0;

	virtual AbstractMailMessageBuilder*
	SetBody(std::string sBody) = 0;

	virtual AbstractMailMessageBuilder*
	SetSubject(std::string sSubject) = 0;

	virtual MailMessage*
	GetResult() = 0;

};