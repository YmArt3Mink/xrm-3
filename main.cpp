#include "Program.h"


int main(int argc, char** argv)
{
	auto prog = Program::GetInstance();
	prog->Run();

	return 0;
}