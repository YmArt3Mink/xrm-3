#pragma once

#include <string>
#include <vector>
#include <algorithm>

#include "AbstractMailMessageBuilder.h"


class MailMessageDirector
{
private:
	AbstractMailMessageBuilder* _pBuilder;

public:
	MailMessageDirector(AbstractMailMessageBuilder* pBuilder) 
		: _pBuilder(pBuilder) {}
	
	~MailMessageDirector() {}

public:
	void
	Build(
		std::vector<std::string> vRecipients,
		std::string				 sBody,
		std::vector<std::string> vRecipientsCopy,
		std::string				 sSubject
		)
	{
		_pBuilder = _pBuilder->SetSubject(sSubject);
		_pBuilder = _pBuilder->SetBody(sBody);
		
		_pBuilder = _pBuilder->ClearRecipients();
		std::for_each(vRecipients.begin(), vRecipients.end(),
			[this] (std::string sRecipient)
		{
			_pBuilder = _pBuilder->AddRecipient(sRecipient);
		});

		_pBuilder = _pBuilder->ClearRecipientsCopy();
		std::for_each(vRecipientsCopy.begin(), vRecipientsCopy.end(),
			[this] (std::string sRecipient)
		{
			_pBuilder = _pBuilder->AddRecipientCopy(sRecipient);
		});
	}

	MailMessage*
	GetResult()
	{
		auto pMessage = _pBuilder->GetResult();
		return pMessage;
	}
};